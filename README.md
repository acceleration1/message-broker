# Privy Acceleration 12 - Message Broker

### This is simple GO Program to use Google Pub Sub


* Add your credential file in 'secret' directory and define the path on secretCred variable on line 38
* Add your Project ID value in variable projectID in line 39
* Add your Topic ID value in variable topicID in line 19
* Execute command 'go run pubsubx.go' to run this program