package main

import (
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"google.golang.org/api/option"
	"log"
)

func main() {
	client, err := newClientPubSub()
	if err != nil {
		log.Fatal(err)
	}

	topicID := ""
	message, err := dummyData()
	if err != nil {
		log.Fatal(err)
	}

	err = messagePublisher(client, topicID, message)
	if err != nil {
		log.Fatal(err)
	}

	if err = topicSubscriber(client, topicID+"-sub"); err != nil {
		log.Fatal(err)
	}
}

func newClientPubSub() (*pubsub.Client, error) {
	var (
		ctx        = context.Background()
		secretCred = ""
		projectID  = ""
	)

	client, err := pubsub.NewClient(ctx, projectID, option.WithCredentialsFile(secretCred))
	if err != nil {
		return nil, fmt.Errorf("pubsub.NewClient: %v", err)
	}
	return client, nil
}

func messagePublisher(client *pubsub.Client, topicID string, data []byte) error {
	ctx := context.Background()

	topic := client.Topic(topicID)
	result := topic.Publish(ctx, &pubsub.Message{
		Data: data,
	})
	_, err := result.Get(ctx)
	if err != nil {
		return fmt.Errorf("Publish: %v", err)
	}
	log.Printf("Published message: %s\n", string(data))
	log.Printf("Success Publish")
	return nil
}

func topicSubscriber(client *pubsub.Client, subscriptionID string) error {
	ctx := context.Background()

	sub := client.Subscription(subscriptionID)
	err := sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
		log.Printf("Received message: %s\n", string(msg.Data))
		msg.Ack()
	})
	if err != nil {
		return fmt.Errorf("sub.Receive: %v", err)
	}

	return nil
}

func dummyData() ([]byte, error) {
	publishData := PublishMessage{
		Key:   uuid.New().String(),
		Value: "New Message",
	}

	payloadByte, err := json.Marshal(publishData)
	if err != nil {
		return nil, err
	}

	return payloadByte, nil
}

type PublishMessage struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
